import java.util.Scanner;

public class Hero extends GameObject {
    Scanner scanner = new Scanner(System.in);

    public Hero(char symbol) {
        setSymbol(symbol);
    }

    public void addRandomLocation(GameScreen screen, Hero hero) {
        int x = 0, y = 2;
        hero.setX(x);
        hero.setY(y);
        screen.setObjectOnLocation(hero, x, y);
    }

    private void moveHero(GameScreen screen, Hero hero, int x, int y, int x_old, int y_old) {
        switch (screen.getObjectOnLocation(x, y)) {
            case '#':
                break;
            case 'E':
                screen.PrintGameCompleted();
                scanner.close();
            default:
                hero.setX(x);
                hero.setY(y);
                screen.setObjectOnLocation(hero, x, y);
                screen.ClearScreenLocation(x_old, y_old);
                break;
        }
    }

    public void moveLeft(GameScreen screen, Hero hero) {
        moveHero(screen, hero, hero.getX() - 1, hero.getY(), hero.getX(), hero.getY());
    }

    public void moveRight(GameScreen screen, Hero hero) {
        moveHero(screen, hero, hero.getX() + 1, hero.getY(), hero.getX(), hero.getY());
    }

    public void moveUp(GameScreen screen, Hero hero) {
        moveHero(screen, hero, hero.getX(), hero.getY() - 1, hero.getX(), hero.getY());
    }

    public void moveDown(GameScreen screen, Hero hero) {
        moveHero(screen, hero, hero.getX(), hero.getY() + 1, hero.getX(), hero.getY());
    }
}