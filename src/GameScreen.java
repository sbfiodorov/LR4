public class GameScreen {
    private final int width;
    private final int height;
    private final char[][] screenMatrix;

    public GameScreen(int width, int height, char[][] screenMatrix) {
        this.width = width;
        this.height = height;
        this.screenMatrix = screenMatrix;
    }

    public void PrintScreen() {
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                System.out.print(this.screenMatrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void PrintGameCompleted() {
        System.out.println("You deserve it");
    }

    public void ClearScreenLocation(int x, int y) {
        this.screenMatrix[y][x] = ' ';
    }

    public char getObjectOnLocation(int x, int y) {
        return this.screenMatrix[y][x];
    }

    public void setObjectOnLocation(GameObject object, int x, int y) {
        this.screenMatrix[y][x] = object.getSymbol();
    }
}