import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {
    private static final String file = "data/input.txt";

    public static void main(String[] args) throws IOException {
        List<String> list = Files.readAllLines(Path.of(file));
        char[][] a = new char[list.size()][];
        for (int i = 0; i < list.size(); i++) {
            a[i] = list.get(i).toCharArray();
        }

        GameScreen screen = new GameScreen(8, 5, a);

        GameElement exit = new GameElement('E');
        screen.setObjectOnLocation(exit, 7, 3);

        Hero hero = new Hero('H');
        hero.addRandomLocation(screen, hero);

        Scanner scanner = new Scanner(System.in);
        char input;
        try {
            while (true) {
                System.out.println("Чтобы остановить игру, нажмите 1 ");
                screen.PrintScreen();
                input = scanner.nextLine().charAt(0);
                switch (input) {
                    case '1':
                        scanner.close();
                        System.exit(1);
                        break;
                    case 'a':
                        hero.moveLeft(screen, hero);
                        break;
                    case 'd':
                        hero.moveRight(screen, hero);
                        break;
                    case 'w':
                        hero.moveUp(screen, hero);
                        break;
                    case 's':
                        hero.moveDown(screen, hero);
                        break;
                    default:
                        break;
                }
            }
        } catch (NoSuchElementException e) {
            System.out.println();
        }
    }
}